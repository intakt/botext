window.Map =
  opening:
    sc:'kezdet'
    title:'Igazi szövegen alapul'
    t: 'Sehogy sem tudok kényelmesen ülni. Vagy a székem szar, vagy én vagyok képtelen. Igaz, hogy sokszor úgy teszek, mintha külső okok gátolnának a célom elérésében, de most talán tényleg egyszerűen csak szar ez a szék. Szinte biztos vagyok benne, hogy ez a helyzet.<br>Kint újra sötét van és korog a gyomrom is megint. Az idő minden lépése kínosan súrlódik a fejemben. Elegem van. Nem tudom pontosan, miért akarok teremteni valamit, ez inkább csak afféle szükség, mint az alvás. De talán mégse olyan, mert alvás nélkül egy idő után elájulnék, de ha nem csinálnék semmit, akkor is addig élnék, amíg van mit ennem. Eddig így volt. Kérdés, hogy érdemes-e csak azért élnem, hogy ehessek. Nem mintha lenne jobb dolgom. Enni amúgy is jó, meg aludni és itt lenni is.<br>     Egy jótékony vákuum vonzásában lebegek. Annyira csinálnék valamit. Talán elég lenne csak gondolnom valamire, mert a gondolat létezés, az választ el attól, amikor nem vagyok. Úgy kellene örülnöm ennek, mintha most kaptam volna karácsonyra.  Miért halogatom mégis a folyamataim? Milyen kedvező környezeti hatásokra várok, ahelyett, hogy elfogadnám, hogy a tudatom az otthonom, egy kipróbált rendszer új generációs példánya, melyben túlteng az erőforrás és a számolási kényszer, amit kihasználatlanul hagyni olyan, mint egyedül bérelni a nászutas lakosztályt.'

  kezdet:
    'megnézem':
      'a képernyőt':'Közelebb hajolok a számítógéphez. Az egyetlen nyitott ablakban egy szöveg várja, hogy hosszabb legyen.'
      'mennyi az idő': 'Az idő szorít, de vehetem úgy, hogy csak nagyon szeret szorosan megölelni. Kínos lenne ha kiderülne, hogy nem muszáj az idő, csak azért van, mert az ember egyedül érezte magát a nagy időtlenségben, ahogy a színészek teszik, amikor megnyomom a pause gombot a lejátszón.'
    'inkább':
      'ennem kéne':'A gyomrom rábír, hogy felkeljek az asztalomtól. Kimegyek a konyhába zöldséget párolni. A kukoricák ragacsosak. Amíg eszem, elindítok egy rajzfilmet. Egy fiatal macska-harcos-tulipán egyenruhában szeli a hullámokat. Egy sötét oldalra állt egyenruha ártatlanok életét fenyegeti. Az utóbbit csak az előbbi győzheti le. <br>     Korábban végzek mint a végkifejlet. Tele vagyok, nem érdekel. Egyáltalán.'
      'lefekszem aludni':
        scene:'alom'
        i:'<br>***<br><br>'
        t:'Amikor felkelek világos az ég alja. Egy japán köntöst viselek és dideregve húzom el az üvegajtót, ami elválaszt a szabadtól. Kilépve meglehetősen furcsának tűnik az ég. Mintha nem lenne elég messze a kép felülete, amin az ég van. '

  alom:
    'mi van':
      'az éggel?':'Mióta van az ég egy óriásplakáton? Érdekes, hogy csak most tűnik ez fel, és bár látom a vonalat, ahol az ég papírja találkozik a kerités túloldalán a földdel, nem tulajdonítok neki különösebb figyelmet. Csak lehetne egy kicsit magasabban, mert így felérik a gyerekek.'
      'a ruhámmal?':'A köntösömön szabálytalan csoportokba rendeződnek apró vonalak. Elnyújtott ütemben keringenek egymással, és mikor jobban elkezdek figyelni rájuk, egyre lassul a mozgásuk.'

    'jön':
      'egy busz':
        t:'Egy busz áll be a kertem végében lévő végállomásra. A kipufofójából komótosan préselődik ki egy hosszú fekete rúd párizsi. <br>Egy bőröndös néni ugrik a járdáról a buszba. '
        set:
          'alom':
            'felszállok':
              'a buszra':
                scene:'busz'
                i:''
                t:'Odafutok a buszhoz és felhúzom magam a hátsó ajtón, ami nyomban berregve bezárul. A jármű felbőg és lassan távolodni kezdünk a betontól. Amikor elérjük az eget halkan kiszakadunk a túloldalra. <br>     Kint sötét van, összeszorul a gyomrom. A feketeségben talpatlan utcalámpák suhannak keresztül. A busz belseje főleg sárgaszínű. A néni sehol sincs, csak a bőröndje árválkodik az egyik ülésen. Úgy érzem magam mint egy elhagyott csomag.'

      'egy vihar':
        t:'Az ég színe először elkezd felázni. A látványa olyan, mintha könnyes lenne a szemem, elszomorodok ahogy rámcsepereg. <br>     Dörgést hallok és összecsukódik a tér.<br><br>A szobámban ébredek.'
        scene:'kezdet'
        set:
          'kezdet':
            'inkább':
              'alszom tovább':
                t:'Becsukom a szemem és egy pillanat alatt újra ott vagyok a kertemben. A dolgok életlennek tűnnek. Az ég fura, a háttérben sziréna szól és valami kerreg.'
                scene:'alom'

  busz:
    'a bőrönd':
      'érdekel':
        scene:'end'
        t:"Ahogy megközelítem a bőröndöt, látom hogy egy kártya van ráragasztva, amin a következő felirat olvasható: <br><br>'Sajnos csak idáig tart ez a szöveg, még ezután lesz valami de aztán vége. Köszönöm, hogy kipróbáltad.'<br><br>Kiváncsi vagyok mit jelenthet ez...<br><br>VÉGE"
      'hidegen hagy':
        scene:'end'
        t:'A bőröndnél sokkal jobban érdekel engem, hogy miért nem látom a buszvezetőt a visszapillantóban. Az émelygésem ellenére előre megyek a fülkéhez. <br>     Nincs senki a volánnál, mi több, kormány sincs és műszerfal se. Ha jobban megnézem, a busznak hiányzik az eleje. Amerre tartunk, éhes sötétség vár.<br>     Talán ha gondolkodom rajta, lesz ott valami.<br><br>VÉGE'
  end:
      ' írta:':
        'plesznivy ákos':''
